import "reflect-metadata";
import {createConnection} from "typeorm";
import * as Koa from 'koa';
import * as bodyparser from 'koa-bodyparser';
import * as http from 'http';
import { router } from './router';
import { Pupils } from "./entity/Pupils";


import config from './config';

createConnection().then(async connection => {
  const app = new Koa();

  app.use(bodyparser({ formLimit: '4mb', jsonLimit: '64mb' }));

  app.use(router.routes());
  app.use(router.routes()).use(router.allowedMethods());


  http.createServer(app.callback()).listen(config.httpServer.port);

  // add pupil to test

  // await connection.manager.save(connection.manager.create(Pupils, {
  //   firstName: "Максим",
  //   lastName: "Курганов",
  //   middleName: "Вячеславович",
  //   dateBirth: new Date('1987-04-19 21:00:00'),
  //   ball: "неуд"
  // }));
  // await connection.manager.save(connection.manager.create(Pupils, {
  //   firstName: "Юлия",
  //   lastName: "Курганова",
  //   middleName: "Фёдоровна",
  //   dateBirth: new Date('1987-12-09  09:00:00'),
  //   ball: "хор"
  // }));

}).catch(error => console.log(error));
