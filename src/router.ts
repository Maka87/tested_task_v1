import * as KoaRouter from 'koa-router';
import * as fs from 'fs';
import * as path from 'path';

const router = new KoaRouter();

const CONTROLLER_DIR: string = path.join(__dirname, 'controllers');
const methods: Array<string> = ["get", "post", "patch", "delete"];

for (const method of methods) {
  const METHOD_CONTROLLER_DIR: string = path.join(CONTROLLER_DIR, method);
  const controllerFiles: Array<string> = fs.readdirSync(METHOD_CONTROLLER_DIR);
  for (const controllerFile of controllerFiles) {
    const controller = require(path.join(METHOD_CONTROLLER_DIR, controllerFile));
    if (typeof controller.default.route == 'string') {
      if (Array.isArray(controller.default.handler)) {
        router[method](controller.default.route, ...controller.default.handler);
      }
      else if (typeof controller.default.handler == 'function') {
        router[method](controller.default.route, controller.default.handler);
      }
    }
  }
}

export {
  router
};

export default router;
