import { Context } from 'koa';
import { pupilController } from '../pupilsController'


async function handler(ctx: Context, next: Function) {
  let pupils = new pupilController();

  let all = await pupils.all(ctx, next);

  ctx.body = all;
}

export default {
  route: '/pupils',
  handler: [handler]
};