import { Context } from 'koa';
import {getManager} from "typeorm";
import { pupilController } from '../pupilsController'

async function handler(ctx: Context, next: Function) {
  if(!ctx.params.id) {
    ctx.status = 404
  }
  
  let pupils = new pupilController();

  let selection = await pupils.one(ctx, next);

  if(!selection) {
    ctx.status = 401
    ctx.body = {
      message: 'not pupil'
    }
  } else {
    let Remove = await pupils.remove(ctx, next);
    if(Remove) {
      ctx.status = 200;
      ctx.body = {result: `OK`};
      return;
    }
    ctx.status = 401;
    ctx.body = {result: `ERROR`};
  }
}

export default {
  route: "/pupils/:id",
  handler: [handler]
};