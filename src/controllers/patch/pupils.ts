import { Context } from 'koa';
import { pupilController } from '../pupilsController'

async function handler(ctx: Context, next: Function) {
  if(!ctx.params.id) {
    ctx.status = 404
  }
  
  let pupils = new pupilController();

  let selection = await pupils.one(ctx, next);

  ctx.request.body.lastName = ctx.request.body.lastName || selection.lastName;
  ctx.request.body.firstName = ctx.request.body.firstName || selection.firstName;
  ctx.request.body.middleName = ctx.request.body.middleName || selection.middleName;
  ctx.request.body.dateBirth = ctx.request.body.dateBirth || selection.dateBirth;
  ctx.request.body.ball = ctx.request.body.ball || selection.ball;
  
  let save = await pupils.save(ctx, next);

  if(save) {
    ctx.status = 200;
    ctx.body = {result: `OK`};
    return;
  }
  ctx.status = 401;
  ctx.body = {result: `ERROR`};
}

export default {
  route: '/pupils/:id',
  handler: [handler]
};