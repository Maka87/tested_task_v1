import { Context } from 'koa';
import { pupilController } from '../pupilsController'
import { iPupil } from '../../interfaces/iPupil';


async function handler(ctx: Context, next: Function) {
  let pupils = new pupilController();
  ctx.status = 401;
  ctx.body = {result: `ERROR`};

  if(!ctx.request.body.lastName) {
    return;
  }
  if(!ctx.request.body.firstName) {
    return;
  }
  if(!ctx.request.body.middleName) {
    ctx.request.body.middleName = ''
  }
  if(!ctx.request.body.dateBirth) {
    return;
  }
  if(!ctx.request.body.ball) {
    ctx.request.body.ball = '';
  }
  
  let save = await pupils.save(ctx, next);

  if(save) {
    ctx.status = 200;
    ctx.body = {result: `OK`};
  }
}

export default {
  route: '/pupils',
  handler: [handler]
};