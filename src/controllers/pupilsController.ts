import { Context } from 'koa';
import { getRepository } from "typeorm";
import { Pupils } from "../entity/Pupils";
import { iPupil } from '../interfaces/iPupil';

export class pupilController {

    private pupilRepository = getRepository(Pupils);

    async all(ctx: Context, next: Function) {
      return this.pupilRepository.find();
    }

    async one(ctx: Context, next: Function) {
        return this.pupilRepository.findOne(ctx.params.id);
    }

    async save(ctx: Context, next: Function) {
      const params: iPupil = {
        lastName: ctx.request.body.lastName,
        firstName: ctx.request.body.firstName,
        middleName: ctx.request.body.middleName ,
        dateBirth: ctx.request.body.dateBirth,
        ball: ctx.request.body.ball || ''
      }
      let pupil: iPupil = {
        lastName: '',
        firstName: '',
        middleName: '',
        dateBirth: new Date(),
        ball: ''
      };
      if (ctx.params.id) {
        let pupilToSelect = await this.pupilRepository.findOne(ctx.params.id);
        pupil = this.pupilRepository.merge(pupilToSelect, params)
      } else {
        pupil = this.pupilRepository.create(params);
      }
      let save = this.pupilRepository.save(pupil);
      if (!save) {
        return false;
      }
      return true;
    }

    async remove(ctx: Context, next: Function) {
      let pupilToRemove = await this.pupilRepository.findOne(ctx.params.id);
      let pupil = await this.pupilRepository.remove(pupilToRemove);
      if (!pupil) {
        return false;
      }
      return true;
    }

}