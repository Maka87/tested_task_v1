export default {
    httpServer: {
        port: 7373,
        baseRoute: '/api'
    }
};
