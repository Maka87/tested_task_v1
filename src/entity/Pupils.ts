import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Pupils {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    middleName: string;

    @Column()
    dateBirth: Date;

    @Column()
    ball: string;

}
