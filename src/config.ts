import defaultConfig from './config/default';
import productionConfig from './config/production';

type Config = {
    httpServer: {
        port: number,
        baseRoute: string
    }
}

let config: Config;

switch (process.env.NODE_ENV) {
    case 'production':
        config = productionConfig;
        break;
    default:
        config = defaultConfig;
        break;
}

export {
    config,
    Config
};

export default config;
